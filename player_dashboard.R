library(shiny)
library(shinydashboard)

library(dplyr)
library(DT)
library(ggplot2)
library(xts)

mpo.lm <- function (data) {
  data <-
    data[, c("StartDate",
             "Tournament.ID",
             "Rating",
             "Tournament.Rating",
             "Place",
             "MPO.pts")]
  data$StartDate <- as.Date(data$StartDate, "%Y-%m-%d")
  data$Tournament.ID <- as.numeric(data$Tournament.ID)
  data$Rating <- as.numeric(data$Rating)
  data$Tournament.Rating <- as.numeric(data$Tournament.Rating)
  data$Place <- as.numeric(data$Place)
  data$MPO.pts <- as.numeric(data$MPO.pts)
  data <- xts(data[, -1], order.by = data$StartDate)
  
  data2 <-
    data.frame(Points = data$MPO.pts, Index = seq_along(data$MPO.pts))
  
  m2 <- lm(MPO.pts ~ log(Index) + 1, data = data2)
  
  intercept.e <- summary(m2)$coefficients[1, 1]
  # intercept.se <- summary(m2)$coefficients[1, 2]
  # intercept.p <- summary(m2)$coefficients[1, 4]
  
  slope.e <- summary(m2)$coefficients[2, 1]
  # slope.se <- summary(m2)$coefficients[2, 2]
  # slope.p <- summary(m2)$coefficients[2, 4]
  
  pred.next <-
    predict(m2, newdata = data.frame(Index = nrow(data2) + 1))
  
  model.fstat.pval <-
    pf(
      summary(m2)$fstatistic[1],
      summary(m2)$fstatistic[2],
      summary(m2)$fstatistic[3],
      lower.tail = FALSE
    )
  
  # return(data.frame(mean.pts = mean(data2$MPO.pts), sd.pts = sd(data2$MPO.pts), pred.next = pred.next, model.fstat.pval = model.fstat.pval, intercept.e = intercept.e, slope.e = slope.e))
  return(
    data.frame(
      mean.pts = mean(data2$MPO.pts),
      sd.pts = sd(data2$MPO.pts),
      pred.next = pred.next,
      model.fstat.pval = model.fstat.pval
    )
  )
}

mpo.plot.lm <- function(data, name) {
  data <- data[data[, "Name"] == name,]
  data <-
    data[, c("StartDate",
             "Tournament.ID",
             "Rating",
             "Tournament.Rating",
             "Place",
             "MPO.pts")]
  
  data$StartDate <- as.Date(data$StartDate, "%Y-%m-%d")
  data$Tournament.ID <- as.numeric(data$Tournament.ID)
  data$Rating <- as.numeric(data$Rating)
  data$Tournament.Rating <- as.numeric(data$Tournament.Rating)
  data$Place <- as.numeric(data$Place)
  data$MPO.pts <- as.numeric(data$MPO.pts)
  data <- xts(data[,-1], order.by = data$StartDate)
  
  data2 <-
    data.frame(Points = data$MPO.pts, Index = seq_along(data$MPO.pts))
  
  plots <- ggplot(data2, aes(x = Index, y = MPO.pts)) +
    geom_point() +
    stat_smooth(method = 'lm', formula = y ~ log(x) + 1) +
    labs(title = paste0("Skip Ace Pts Trendline for ", name)) +
    ylim(-250, 250)
  
  plots
}

setup <- function() {
  print("Loading Data....")
  df <- read.csv("data/NT_ES_M_XM_mpo_results.csv")
  mpo.players <- read.csv("data/mpo_tour_cards.csv")
  rosters <- read.csv("data/rosters.csv")
  print("Data loaded.")
  
  model.preds <- df %>% group_by(PDGA) %>% do(mpo.lm(.))
  counts <- df %>% count(PDGA)
  
  results <- merge(mpo.players, counts, by = "PDGA")
  results <- merge(results, model.preds, by = "PDGA")
  
  others.players <- c(rosters$James, rosters$Ryan, rosters$Haden)
  available.players <-
    results[!(results[, "Player.Name"] %in% others.players),]
  available.players$MINE <-
    available.players[, "Player.Name"] %in% rosters$Parker
}

ui <- dashboardPage(
  dashboardHeader(title = "Player Comparison Dashboard"),
  dashboardSidebar(
    uiOutput("select.player1"),
    uiOutput("select.player2"),
    uiOutput("select.player3"),
    uiOutput("select.player4")
  ),
  dashboardBody(tabsetPanel(
    tabPanel(
      "Player Comparisons",
      column(6,
             plotOutput("plot1"),
             br(),
             plotOutput("plot3")),
      column(6,
             plotOutput("plot2"),
             br(),
             plotOutput("plot4"))
    ),
    tabPanel(
      "Player Data",
      uiOutput("player.data")
    )
  ))
)

server <- function(input, output) {
  print("Loading Data....")
  df <- read.csv("data/NT_ES_M_XM_mpo_results.csv")
  mpo.players <- read.csv("data/mpo_tour_cards.csv")
  rosters <- read.csv("data/rosters.csv")
  print("Data loaded.")
  
  model.preds <- df %>% group_by(PDGA) %>% do(mpo.lm(.))
  counts <- df %>% count(PDGA)
  
  results <- merge(mpo.players, counts, by = "PDGA")
  results <- merge(results, model.preds, by = "PDGA")
  
  others.players <- c(rosters$James, rosters$Ryan, rosters$Haden)
  available.players <-
    results[!(results[, "Player.Name"] %in% others.players),]
  available.players$MINE <-
    available.players[, "Player.Name"] %in% rosters$Parker
  
  output$player.data <- renderUI({DT::renderDT(available.players)})
  
  output$select.player1 <- renderUI({
    selectInput("player1", "Player 1", mpo.players)
  })
  
  output$select.player2 <- renderUI({
    selectInput("player2", "Player 2", mpo.players)
  })
  
  output$select.player3 <- renderUI({
    selectInput("player3", "Player 3", mpo.players)
  })
  
  output$select.player4 <- renderUI({
    selectInput("player4", "Player 4", mpo.players)
  })
  
  output$plot1 <- renderPlot({
    mpo.plot.lm(df, input$player1)
  })
  
  output$plot2 <- renderPlot({
    mpo.plot.lm(df, input$player2)
  })
  
  output$plot3 <- renderPlot({
    mpo.plot.lm(df, input$player3)
  })
  
  output$plot4 <- renderPlot({
    mpo.plot.lm(df, input$player4)
  })
}

shinyApp(ui, server, setup)